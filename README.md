---
Title: Διαχείριση εξυπηρετητή linux, με το aaPanel. Μέρος 1ο.
Alternative Title: Linux server management, with aaPanel. Part 1.
Contributor: @nikaskonstantinos
Language: el
Date Issued: 14/11/2021
Publisher: Linux User Forum – Το forum των Ελλήνων χρηστών Linux – https://linux-user.gr/
Description: How to install aapanel for the purpose of managing a linux server, with a graphical interface. Beginner's guide in Greek.
Category: Άρθρα και οδηγοί
Tags: aaPanel, linux, server

---

Εισαγωγή:

Στο πρώτο μέρος του οδηγού που ακολουθεί, θα δούμε πως γίνεται η εγκατάσταση του **aaPanel** σε έναν linux εξυπηρετητή.

Το [aaPanel](https://www.aapanel.com/reference.html) είναι ένα δωρεάν ανοικτού κώδικα λογισμικό, με το οποίο μπορούμε να εγκαταστήσουμε σε έναν linux  εξυπηρετητή, εφαρμογές με μερικά κλικ, όπως για παράδειγμα LNMP, LAMP server, Fileserver, FTPserver, Wordpress, Drupal, Joomla, Roundcube και άλλα πολλά, μέσα από ένα φιλικό  web-based περιβάλλον, στον φυλλομετρητή μας.

Η χρήση του aaPanel είναι σχετικά απλή ακόμη και για αρχάριους χρήστες και η εγκατάσταση του μπορεί να γίνει και σε Raspberry Pi 4.
Προσοχή: Η εγκατάσταση πρέπει να γίνει σε μια καθαρή εγκατάσταση εξυπηρετητή στον οποίο δεν προϋπάρχουν mysql, apache2, php, nginx, phpmyadmin.

Σε περίπτωση (Debian/Ubuntu) ήδη υπάρχουσας εγκατάστασης μιας από τις παραπάνω εφαρμογές, για παράδειγμα της mysql, την αφαιρούμε με τις εντολές:

```bash
sudo apt purge [εφαρμογή]*
```

```bash
sudo apt autoclean
```



Εγκατάσταση:

Η εγκατάσταση του aaPanel είναι πολύ απλή (μία εντολή) και διαρκεί γύρω στα 20 λεπτά ή περισσότερο ανάλογα με την ταχύτητα του δικτύου μας.

Για Debian/Ubuntu διανομές η εντολή είναι η παρακάτω:

```bash
wget -O install.sh http://www.aapanel.com/script/install-ubuntu_6.0_en.sh && sudo bash install.sh 66959f96

```

Απεγκατάσταση:

```bash
service bt stop && chkconfig --del bt && rm -f /etc/init.d/bt && rm -rf /www/server/panel
```

Χρήσιμες εντολές:

https://www.aapanel.com/reference.html

Εικόνες:

Εικόνα 1. ![Login page](https://gitlab.com/nikaskonstantinos/aapanel_post/-/raw/main/images/aapanel01-2021-11-14_18-03-34.png)
Εικόνα 2. ![Terminal page](https://gitlab.com/nikaskonstantinos/aapanel_post/-/raw/main/images/aapanel02-2021-11-14_18-02-52.png)
Εικόνα 3. ![one-click LNMP/LAMP installations](https://gitlab.com/nikaskonstantinos/aapanel_post/-/raw/main/images/aapanel03-2021-11-14_17-59-52.png)
Εικόνα 4. ![server home page](https://gitlab.com/nikaskonstantinos/aapanel_post/-/raw/main/images/aapanel04-2021-11-14_18-11-57.png)



Πηγές:

1. https://www.aapanel.com/feature.html
2. https://github.com/aaPanel/aaPanel
3. https://gitlab.com/nikaskonstantinos/aapanel_post
4. https://linux-user.gr/t/diacheirish-exyphrethth-linux-me-to-aapanel-meros-1o/3630

